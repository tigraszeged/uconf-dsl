enablePlugins(SbtJsonPlugin)

jsonInterpreter := plainCaseClasses.withPlayJsonFormats

jsValueFilter := allJsValues.exceptEmptyArrays

libraryDependencies += "io.circe" %% "circe-generic" % "0.9.0"

libraryDependencies += "javax.mail" % "mail" % "1.4.7"
