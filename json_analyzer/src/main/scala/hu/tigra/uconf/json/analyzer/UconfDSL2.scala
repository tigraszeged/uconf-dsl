package hu.tigra.uconf.json.analyzer

import java.io.{BufferedWriter, File, FileWriter}

import io.circe.Printer

import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers


// JSON adatszerkezet

// A kapott JSON-ben üres az mxLepesCsoport lista
case class Root(munkafolyamatLepesek: List[Lépés], munkatarsCsoportok: List[Név], mxLepesCsoport: List[String], ugyletTipusok: List[UgyletTipus], urlapok: List[ŰrlapSablon])

case class UgyletTipus(lepesek: List[String], munkatarsCsoportok: List[String], nev: String, ugyletTipusAdatok: List[UgylettipusAdat])

case class Lépés(kod: String, nev: String)

case class ŰrlapSablon(nev: String, xhtmlBackingBean: Option[String] = None) // todo xhtmlBackingBean


// Belső adatszerkezet

case class ÜgyletAdatDSL(
                          név: Név,
                          lépések: Lépések,
                          csoportok: Csoportok,
                          konfigok: Konfigok,
                          nézetek: Nézetek,
                          űrlapok: ŰrlapLista,
                          extraSzabályok: List[ExtraSzabályok]
                        )

case class Név(nev: String)

case class Lépések(l: List[String])

case class Csoportok(l: List[String])

case class Konfigok(l: List[String])

case class Nézetek(l: List[String])

case class ŰrlapFa(név: String, szülő: String, gyerekek: List[ŰrlapFa])

case class Űrlap(név: String, szülő: String)

case class ŰrlapLista(l: List[Űrlap])

case class ExtraSzabályok(űrlap: String, nézetMód: String, konfigMód: Konfigok, lépések: Lépések, csoportok: Csoportok)

case class UgylettipusAdat(egyediKod: String, konfig: List[Konfig], szulo: Option[String], szuloEgyediKod: Option[String], urlap: String)

case class Konfig(konfigKey: KonfigKey, konfigMod: KonfigMod, nezetMod: String)

case class KonfigKey(munkafolyamatLepes: Option[String], munkatarsCsoport: Option[String])

case class KonfigMod(default: Boolean, megjegyzesLathato: Boolean, megjegyzesSzerkeszt: Boolean, nemMentendo: Boolean)

object UconfDsl2 {

  def main(args: Array[String]): Unit = {

    val root = readFile("json_analyzer/src/main/resources/dsl/root.txt")
    val ügyletTípus1 = readFile("json_analyzer/src/main/resources/dsl/teszt urlap.txt")

    val ügyletAdatDSLData: ÜgyletAdatDSL = MyParser.parse(ügyletTípus1)
    val jsonData = dslToData(ügyletAdatDSLData)

    val rootData: ÜgyletAdatDSL = MyParser.parse(root)
    val rootJsonData = dslRootToJsonData(rootData, jsonData)

    /*
        println(jsonData)
        println("Ügylettípus adatok " + jsonData.ugylettipusAdatok.length + " db")
        println("Űrlapkonfig / ügylettípus " + jsonData.ugylettipusAdatok.head.konfig.length + " db")
    */
    import io.circe.syntax._
    import io.circe.generic.auto._

    // dropNullValues jelentése: nem jelennek meg a None típusú értékeket tartalmazó mezők.
    // Ilyen értéket csak az Option típusú mezők tartalmazhatnak.

    writeFile(
      Printer.spaces2.copy(dropNullValues = true).pretty(
        rootJsonData.asJson
      )
      ,
      "json_analyzer/src/main/resources/json/dsl-kimenet.json"
    )
  }

  def readFile(path: String): String = Source.fromFile(path).getLines().mkString("\n")

  def writeFile(content: String, path: String): Unit = {
    val f = new File(path)
    val writer = new BufferedWriter(new FileWriter(f))
    writer.write(content)
    writer.close()
  }

  object MyParser extends JavaTokenParsers {

    def parse(s: String): ÜgyletAdatDSL = parseAll(MyParser.dsl, s) match {
      case Success(startDslData, _) =>
        startDslData

      case NoSuccess(msg, next) =>
        println(msg)
        println(next)
        ÜgyletAdatDSL(null, null, null, null, null, null, null)
    }

    def sorVége = "."

    def betűk = """[\w\-á-ű]+""".r

    def szó = betűk | "\"" ~> rep1(betűk) <~ "\"" ^^ {
      list => list.mkString(" ")
    }

    def űrlap = """\+*[\w\-á-ű]+\.*\w*""".r

    def szóLista = rep1(szó) <~ sorVége

    def dsl = név ~ lépésLista ~ opt(munkatársCsoportLista) ~ konfigMódLista ~ nézetMódLista ~ űrlapFa into {
      case n ~ l ~ m ~ km ~ nm ~ űf =>
        rep(általánosExtraSzabályok(nm, km) | extraSzabályok(nm, km, l, m.getOrElse(Csoportok(Nil)))) ^^ {
          eszList => ÜgyletAdatDSL(n, l, m.getOrElse(Csoportok(Nil)), km, nm, ŰrlapLista(űrlapFaToList(űf.gyerekek)), eszList)
        }
    }

    def név = "azonosító" ~ ":" ~> szó <~ sorVége ^^ {
      s => Név(s)
    }

    def lépésLista = "lépések" ~ ":" ~> szóLista ^^ {
      l => Lépések(l)
    }

    def munkatársCsoportLista = "munkatárs" ~ "csoportok" ~ ":" ~> szóLista ^^ {
      l => Csoportok(l)
    }

    def konfigMódLista = "konfig" ~ "mód" ~ ":" ~> szóLista ^^ {
      l => Konfigok(l)
    }

    def nézetMódLista = "nézet" ~ "mód" ~ ":" ~> szóLista ^^ {
      l => Nézetek(l)
    }

    def űrlapFa = "űrlapok" ~ ":" ~> rep1("+" ~> űrlap) ^^ {
      sList => ŰrlapFa("root", "", parseTree(sList))
    }

    def általánosExtraSzabályok(nézetek: Nézetek, konfigok: Konfigok) =
      űrlap ~ opt(szóListából(nézetek.l)) ~ rep(szóListából(konfigok.l)) <~ "mindenhol" ^^ {
        case űrlap ~ nézetek ~ konfigok =>
          nézetek match {
            case Some(n) =>
              ExtraSzabályok(űrlap, n, Konfigok(konfigok), Lépések(Nil), Csoportok(Nil))
            case None =>
              ExtraSzabályok(űrlap, "", Konfigok(konfigok), Lépések(Nil), Csoportok(Nil))
          }
      }

    def extraSzabályok(nézetek: Nézetek, konfigok: Konfigok, lépések: Lépések, csoportok: Csoportok) =
      űrlap ~ opt(szóListából(nézetek.l)) ~ rep(szóListából(konfigok.l)) ~ rep(szóListából(lépések.l)) ~ rep(szóListából(csoportok.l)) ^^ {
        case űrlap ~ nézetek ~ konfigok ~ lépések ~ csoportok =>
          nézetek match {
            case Some(n) =>
              ExtraSzabályok(űrlap, n, Konfigok(konfigok), Lépések(lépések), Csoportok(csoportok))
            case None =>
              ExtraSzabályok(űrlap, "", Konfigok(konfigok), Lépések(lépések), Csoportok(csoportok))
          }
      }

    def űrlapFaToList(faLista: List[ŰrlapFa], list: List[Űrlap] = Nil): List[Űrlap] = faLista match {
      case Nil => list
      case ŰrlapFa(név, szülő, gyerekek) :: többi =>
        val ez = Űrlap(név, szülő)
        űrlapFaToList(többi, űrlapFaToList(gyerekek, list ::: List(ez)))
    }

    val űrlapFaNode = """(\+*)(\+*[\w\-á-ű]+\.*\w*)""".r

    def parseTree(src: List[String], root: List[ŰrlapFa] = Nil): List[ŰrlapFa] = src match {
      case Nil => root
      case elso :: többiString =>
        elso match {
          case űrlapFaNode(pluszok, név) =>
            parseTree(többiString, addToNthChild(ŰrlapFa(név, "", Nil), pluszok.length, root))
        }
    }

    def addToNthChild(ű: ŰrlapFa, n: Int, list: List[ŰrlapFa]): List[ŰrlapFa] = n match {
      case 0 =>
        list ::: List(ű)
      case 1 =>
        val tmpLast = list.last
        val módosítottGyerek = ŰrlapFa(ű.név, tmpLast.név, ű.gyerekek)
        val módosítottSzülő = ŰrlapFa(tmpLast.név, tmpLast.szülő, tmpLast.gyerekek ::: List(módosítottGyerek))
        list.dropRight(1) ::: List(módosítottSzülő)
      case _ =>
        list.dropRight(1) ::: List(ŰrlapFa(list.last.név, list.last.szülő, addToNthChild(ű, n - 1, list.last.gyerekek)))
    }

    def szóListából(lepesek: List[String]) = szó ^? {
      case lepes if lepesek.contains(lepes) => lepes
    }
  }

  def dslRootToJsonData(rootDsl: ÜgyletAdatDSL, ugyletTípus: UgyletTipus): Root = rootDsl match {
    case ÜgyletAdatDSL(név, lépések, csoportok, konfigok, nézetek, űrlapok, _) =>
      Root(lépések.l.map(id => Lépés(id, id)), csoportok.l.map(Név), List.empty[String], List(ugyletTípus), űrlapok.l.map(ű => ŰrlapSablon(ű.név)))
  }

  def dslToData(ugyletDsl: ÜgyletAdatDSL): UgyletTipus = ugyletDsl match {
    case ÜgyletAdatDSL(Név(név), Lépések(lépések), Csoportok(csoportok), Konfigok(konfigok), Nézetek(nézetek), ŰrlapLista(űrlapok), extraSzabalyok) =>
      UgyletTipus(lépések, csoportok, név, for (Űrlap(név, szülő) <- űrlapok) yield {
        UgylettipusAdat(név, for (
          lepes <- if (lépések.nonEmpty) lépések.map(Some(_)) else List(None);
          csoport <- if (csoportok.nonEmpty) csoportok.map(Some(_)) else List(None)
        ) yield {
          val defaultKonf = (false, false, false, false, nézetek.head)
          val konf = (for (extra <- extraSzabalyok) yield {
            decodeExtraSzabalyok(név, lepes.orNull, csoport.orNull, nézetek.head, extra)
          })
            .foldLeft(defaultKonf)((x, y) => {
              (x._1 || y._1, x._2 || y._2, x._3 || y._3, x._4 || y._4,
                if (!y._1) y._5 else x._5 // ha y nem default
              )
            })
          Konfig(
            KonfigKey(lepes, csoport),
            KonfigMod(konf._1, konf._2, konf._3, konf._4),
            konf._5
          )
        }, if (szülő.nonEmpty) Some(szülő) else None, if (szülő.nonEmpty) Some(szülő) else None, név)
      })
  }

  def decodeExtraSzabalyok(urlap: String, lepes: String, csoport: String, defaultNezetMod: String, szabaly: ExtraSzabályok) = szabaly match {
    case ExtraSzabályok(u, nm, km, l, cs) =>
      if (u == urlap && (l.l.isEmpty || l.l.contains(lepes)) && (cs.l.isEmpty || cs.l.contains(csoport))) {
        (km.l.contains("default"), km.l.contains("megjegyzesLathato"), km.l.contains("megjegyzesSzerkeszt"), km.l.contains("nemMentendo"), nm)
      } else {
        (false, false, false, false, defaultNezetMod)
      }
  }
}
