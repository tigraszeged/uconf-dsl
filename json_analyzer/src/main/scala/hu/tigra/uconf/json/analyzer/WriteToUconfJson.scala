package hu.tigra.uconf.json.analyzer

import hu.tigra.uconf.json.analyzer.UconfDsl2.dslToData
import io.circe.syntax._

object WriteToUconfJson {

  def main(args: Array[String]): Unit = {

    val test1 =
      """
        |azonosító: BESZ.
        |
        |lépések: BESZ020 BESZ030.
        |
        |munkatárs csoportok: csop2 csop3.
        |
        |konfig mód: default megjegyzesLathato megjegyzesSzerkeszt nemMentendo.
        |
        |nézet mód: Látható "Nem Látható" Módosítható.
        |
        |űrlapok:
        |    +Szülő
        |    ++Alszülő
        |    +++urlap.xhtml
        |    ++Alszülő2
        |    +++urlap2.xhtml
        |    +urlap3.xhtml
        |    +urlap4.xhtml
        |
        |urlap.xhtml default mindenhol
        |urlap2.xhtml default BESZ020
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo mindenhol
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo BESZ030
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo BESZ020 csop3
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo csop3
        |urlap.xhtml "Nem Látható" BESZ020 csop2 csop3
        |
        |""".stripMargin

    val ügyletAdat = UconfDsl2.MyParser.parse(test1)

    import io.circe.generic.auto._
    val jsonData = dslToData(ügyletAdat)

    println(
      jsonData.asJson
    )
  }
}