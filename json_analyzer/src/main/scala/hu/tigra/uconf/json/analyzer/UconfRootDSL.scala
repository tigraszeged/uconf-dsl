package hu.tigra.uconf.json.analyzer

import java.io._

import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers


object UconfRootDSL {

  // JSON adatszerkezet

  // A kapott JSON-ben üres az mxLepesCsoport lista
  case class Root(munkafolyamatLepesek: List[Lépés], munkatarsCsoportok: List[Név], mxLepesCsoport: List[String], ugyletTipusok: List[UgyletTipus], urlapok: List[ŰrlapSablon])

  case class UgyletTipus(lepesek: List[String], munkatarsCsoportok: List[String], nev: String, ugyletTipusAdatok: List[UgylettipusAdat])

  case class Lépés(kod: String, nev: String)

  case class Csoport(nev: String)

  case class ŰrlapSablon(nev: String, xhtmlBackingBean: Option[String] = None) // todo xhtmlBackingBean


  // Belső adatszerkezet

  case class ÜgyletAdatDSL(
                            név: Név,
                            lépések: Lépések,
                            csoportok: Csoportok,
                            konfigok: Konfigok,
                            nézetek: Nézetek,
                            űrlapok: ŰrlapLista,
                            extraSzabályok: List[ExtraSzabályok]
                          )

  case class Név(nev: String)

  case class Lépések(l: List[String])

  case class Csoportok(l: List[String])

  case class Konfigok(l: List[String])

  case class Nézetek(l: List[String])

  case class ŰrlapFa(név: String, szülő: String, gyerekek: List[ŰrlapFa])

  case class Űrlap(név: String, szülő: String)

  case class ŰrlapLista(l: List[Űrlap])

  case class ExtraSzabályok(űrlap: String, nézetMód: String, konfigMód: Konfigok, lépések: Lépések, csoportok: Csoportok)

  case class UgylettipusAdat(egyediKod: String, konfig: List[Konfig], szulo: Option[String], szuloEgyediKod: Option[String], urlap: String)

  case class TömörUgylettipusAdat(egyediKod: String, konfig: List[TömörKonfig], szulo: Option[String], szuloEgyediKod: Option[String], urlap: String)

  class Konfig(konfigKey: KonfigKey, konfigMod: KonfigMod, nezetMod: String)

  class TömörKonfig(val konfigKey: TömörKonfigKey, val konfigMod: KonfigMod, val nezetMod1: Char, val nezetMod2: Char)

  class KonfigKey(munkafolyamatLepes: Option[String], munkatarsCsoport: Option[String])

  class TömörKonfigKey(val munkafolyamatLepesIndex: Int, val munkatarsCsoportIndex: Byte)

  class KonfigMod(val default: Boolean, val megjegyzesLathato: Boolean, val megjegyzesSzerkeszt: Boolean, val nemMentendo: Boolean)


  def main(args: Array[String]): Unit = {

    val szűrt = readFile("json_analyzer/src/main/resources/json/UrlapKonfigR1TG_szurt.json")

    val példaJsonString =
      """{
        |  "munkafolyamatLepesek" : [ {
        |    "kod" : "BESZ010",
        |    "nev" : "Szakmai beszámoló előkészítése"
        |  }, {
        |    "kod" : "BESZ020",
        |    "nev" : "Szakmai beszámoló szerkesztése"
        |  } ],
        |  "munkatarsCsoportok" : [ {
        |    "nev" : "SZAKTERULETI_KONTROLLER_EMMI"
        |  }, {
        |    "nev" : "SZERVEZET_BETEKINTO"
        |  }, {
        |    "nev" : "SZERVEZET_BETEKINTO_EMMI"
        |  }, {
        |    "nev" : "SZERVEZET_UGYINTEZO"
        |  }, {
        |    "nev" : "SZERVEZET_UGYINTEZO_EMMI"
        |  }, {
        |    "nev" : "SZERVEZET_VEZETO"
        |  }, {
        |    "nev" : "SZERVEZET_VEZETO_EMMI"
        |  } ],
        |  "mxLepesCsoport" : [ ],
        |  "ugyletTipusok" : [ {
        |    "lepesek" : [ "BESZ010", "BESZ020", "BESZ030", "BESZ050", "BESZ060", "BESZ070", "BESZ080", "BESZ090", "BESZEJH010", "BESZEJH020", "BESZEJH030", "BESZHP010", "BESZHP020", "BESZHP030", "BESZHP040", "BESZHP050", "BESZHP060", "BESZHP200", "BESZHPEJH010", "BESZHPEJH020", "BESZHPEJH030", "BESZHPHJH010", "BESZHPHJH020", "BESZHPHJH030", "BESZKJH010", "BESZKJH020", "BESZKJH030", "BESZEERT010", "BESZEERT020", "BESZEERT030", "BESZEERTJH010", "BESZEERTJH020", "BESZEERTJH030", "UGYLET" ],
        |    "munkatarsCsoportok" : [ "SZERVEZET_BETEKINTO", "SZERVEZET_BETEKINTO_EMMI", "SZERVEZET_UGYINTEZO", "SZERVEZET_UGYINTEZO_EMMI", "SZERVEZET_VEZETO", "SZERVEZET_VEZETO_EMMI" ],
        |    "nev" : "BESZ",
        |    "ugyletTipusAdatok" : [ {
        |      "egyediKod" : "/fedlap/fedlap.xhtml",
        |      "konfig" : [ {
        |        "konfigKey" : {
        |          "munkafolyamatLepes" : "BESZ060",
        |          "munkatarsCsoport" : "SZERVEZET_VEZETO_EMMI"
        |        },
        |        "konfigMod" : {
        |          "default" : false,
        |          "megjegyzesLathato" : false,
        |          "megjegyzesSzerkeszt" : false,
        |          "nemMentendo" : false
        |        },
        |        "nezetMod" : "Módosítható"
        |      } ] """.stripMargin

    val allFalseLáthatóKonfigMód =
      """, \{
        |        "konfigKey" : \{
        |          "munkafolyamatLepes" : ".+",
        |          "munkatarsCsoport" : ".+"
        |        \},
        |        "konfigMod" : \{
        |          "default" : false,
        |          "megjegyzesLathato" : false,
        |          "megjegyzesSzerkeszt" : false,
        |          "nemMentendo" : false
        |        \},
        |        "nezetMod" : "Látható"
        |      \}""".stripMargin.r

    /*
        val szűrt = allFalseLáthatóKonfigMód.replaceAllIn(root, "")

        println("teljes JSON",root.length)
        println("szűrt JSON",szűrt.length)

        val szűrtFile = new PrintWriter(new File("json_analyzer/src/main/resources/json/UrlapKonfigR1TG_szurt.json"))
        szűrtFile.write(szűrt)
        szűrtFile.flush()
        szűrtFile.close()
    */

    val (lépések, csoportok, ügyletek) = MyParser.parse(
      példaJsonString
    )
    println(lépések.size)
    println(lépések)
    println(csoportok.size)
    println(csoportok)
    println(ügyletek.size)
    println(ügyletek)

    /*
        val ügyletAdatDSLData: ÜgyletAdatDSL = MyParser.parse(ügyletTípus1)
        val jsonData = dslToData(ügyletAdatDSLData)

        val rootData: ÜgyletAdatDSL = MyParser.parse(root)
        val rootJsonData = dslRootToJsonData(rootData, jsonData)

        import io.circe.syntax._
        import io.circe.generic.auto._

        // dropNullValues jelentése: nem jelennek meg a None típusú értékeket tartalmazó mezők.
        // Ilyen értéket csak az Option típusú mezők tartalmazhatnak.

        writeFile(
          Printer.spaces2.copy(dropNullValues = true).pretty(
            rootJsonData.asJson
          )
          ,
          "json_analyzer/src/main/resources/json/dsl-kimenet.json"
        )
    */
  }

  def readFile(path: String): String = Source.fromFile(path).getLines().mkString("\n")

  def writeFile(content: String, path: String): Unit = {
    val f = new File(path)
    val writer = new BufferedWriter(new FileWriter(f))
    writer.write(content)
    writer.close()
  }

  object MyParser extends JavaTokenParsers {

    var lépésekRoot = Array.empty[String]
    var csoportokRoot = Array.empty[String]

    def parse(s: String): (List[Lépés], List[Csoport], List[(List[String], List[String], String, List[TömörUgylettipusAdat])]) =
      parse(MyParser.rootDsl, s) match {
        case Success(data, _) =>
          data

        case NoSuccess(msg, next) =>
          println(msg)
          println(next)
          null
      }


    def rootDsl = maybeInBraces(
      lépések ~ opt(",") ~
        csoportok ~ opt(",") ~
        log(mxLepesCsoport)("mxLepesCsoport") ~ opt(",") ~
        ügyletek ~ opt(",")
    ) ^^ {
      case lk ~ comma1 ~ csk ~ c2 ~ mx ~ c3 ~ utk ~ c4 => (lk, csk, /* mx, */ utk)
    }


    //def anythingElse = rep("""[^\n]""".r)

    def property[A](key: String) = maybeInQuotes(key) ~ ":" ~> miq_szó

    def property_comma[A](key: String) = property(key) <~ opt(",")

    def maybeInQuotes[A](p: Parser[A]) = opt("\"") ~> p <~ opt("\"")

    def maybeInBraces[A](p: Parser[A]) = opt("{") ~> p <~ opt("}")

    def maybeInBrackets[A](p: Parser[A]) = opt("[") ~> p <~ opt("]")


    def ügyletTipusAdatok =
      maybeInQuotes("ugyletTipusAdatok") ~ ":" ~> maybeInBrackets(repsep(ügyletTipusAdat, opt(",")))

    def ügyletTipusAdat = maybeInBraces(
      property_comma("egyediKod") ~
        konfigLista ~ opt(",") ~
        opt(property_comma("szulo") ~ property_comma("szuloEgyediKod")) ~
        property("urlap")
    ) ^^ {

      case ekód ~ konf ~ comma ~ sz_szekód ~ űrl =>
        val (sz, szekód) = sz_szekód match {
          case Some(sz ~ szekód) => (Some(sz), Some(szekód))
          case None => (None, None)
        }
        TömörUgylettipusAdat(
          egyediKod = ekód, konfig = konf, szulo = sz, szuloEgyediKod = szekód, urlap = űrl
        )
    }

    def konfigLista = "konfig" ~ ":" ~> maybeInBrackets(repsep(konfig, opt(",")))

    def konfig = maybeInBraces(konfigKey ~ konfigMod ~ nézetMód) ^^ {
      case kk ~ km ~ nm =>
        val nmChars = nm.toCharArray.filter(c => c > 'A')
        new TömörKonfig(konfigKey = kk, konfigMod = km, nezetMod1 = nmChars(0), nezetMod2 = nmChars(1))
    }

    def konfigKey = "konfigKey" ~ ":" ~> maybeInBraces(
      property_comma("munkafolyamatLepes") ~ property("munkatarsCsoport")
    ) ^^ {
      case l ~ cs => new TömörKonfigKey(
        munkafolyamatLepesIndex = lépésekRoot.indexOf(l), munkatarsCsoportIndex = csoportokRoot.indexOf(cs).toByte
      )
    }

    def konfigMod = "konfigMod" ~ ":" ~> maybeInBraces(
      property_comma("default") ~ property_comma("megjegyzesLathato") ~
        property_comma("megjegyzesSzerkeszt") ~ property("nemMentendo")
    ) ^^ {
      case d ~ ml ~ msz ~ nm => new KonfigMod(
        default = d == "true",
        megjegyzesLathato = ml == "true",
        megjegyzesSzerkeszt = msz == "true",
        nemMentendo = nm == "true"
      )
    }

    def nézetMód = maybeInQuotes("nezetMod") ~ ":" ~> miq_szó


    def ügyletTipus = log(lépésIdk)("lépésIdk") ~ opt(",") ~ csoportIdk ~ opt(",") ~ név ~ opt(",") ~ ügyletTipusAdatok ^^ {
      case lépk ~ comma1 ~ csopk ~ c2 ~ név ~ c3 ~ ügylet => (lépk, csopk, név, ügylet)
    }

    def lépésIdk = maybeInQuotes("lepesek") ~ ":" ~> maybeInBrackets(repsep(log(szó)("szó"), opt(",")))

    def csoportIdk = maybeInQuotes("munkatarsCsoportok") ~ ":" ~> maybeInBrackets(repsep(szó, opt(",")))

    def név = maybeInQuotes("nev") ~ ":" ~> miq_szó


    def lépések = maybeInQuotes("lépések" | "munkafolyamatLepesek") ~ ":" ~>
      maybeInBrackets(
        repsep(no_log(maybeInBraces(munkafolyamatLepes))("munkafolyamatLepes"), opt(","))
      ) ^^ {
      lk =>
        lépésekRoot = lk.map(_.kod).toArray
        lk
    }

    def csoportok = maybeInQuotes("csoportok" | "munkatarsCsoportok") ~ ":" ~>
      maybeInBrackets(
        repsep(no_log(maybeInBraces(munkatarsCsoport))("munkafolyamatLepes"), opt(","))
      ) ^^ {
      csk =>
        csoportokRoot = csk.map(_.nev).toArray
        csk
    }

    def mxLepesCsoport = maybeInQuotes("mxLepesCsoport") ~ ":" ~ "[" ~ "]"

    def ügyletek = maybeInQuotes("ügyletek" | "ugyletTipusok") ~ ":" ~> maybeInBrackets(repsep(maybeInBraces(log(ügyletTipus)("ügyletTipus")), opt(",")))


    def munkafolyamatLepes = maybeInQuotes("kod") ~> ":" ~> no_log(maybeInQuotes(betűk))("betűk") ~ opt(",") ~ maybeInQuotes("nev") ~ ":" ~ miq_szó ^^ {
      case kodValue ~ comma ~ nev ~ colon ~ nevValue => Lépés(kodValue, nevValue)
    }

    def munkatarsCsoport = maybeInQuotes("nev") ~ ":" ~ miq_szó ^^ {
      case nev ~ colon ~ value => Csoport(value)
    }

    def miq_szó = maybeInQuotes(rep(betűk)) ^^ {
      list => list.mkString(" ")
    }

    def betűk = """[\w\-\(\)\./áíűőüöúóéÁÍŰŐÜÖÚÓÉ]+""".r

    def no_log[A](p: Parser[A])(notUsed: String) = p


    def sorVége = "."

    def szó = betűk | "\"" ~> rep1(betűk) <~ "\"" ^^ {
      list => list.mkString(" ")
    }

    def űrlap = """\+*[\w\-á-ű]+\.*\w*""".r

    def szóLista = rep1(szó) <~ sorVége


    def dsl = azonosító ~ lépésLista ~ opt(munkatársCsoportLista) ~ konfigMódLista ~ nézetMódLista ~ űrlapFa into {
      case n ~ l ~ m ~ km ~ nm ~ űf =>
        rep(általánosExtraSzabályok(nm, km) | extraSzabályok(nm, km, l, m.getOrElse(Csoportok(Nil)))) ^^ {
          eszList => ÜgyletAdatDSL(n, l, m.getOrElse(Csoportok(Nil)), km, nm, ŰrlapLista(űrlapFaToList(űf.gyerekek)), eszList)
        }
    }

    def azonosító = "azonosító" ~ ":" ~> szó <~ sorVége ^^ {
      s => Név(s)
    }

    def lépésLista = "lépések" ~ ":" ~> szóLista ^^ {
      l => Lépések(l)
    }

    def munkatársCsoportLista = "munkatárs" ~ "csoportok" ~ ":" ~> szóLista ^^ {
      l => Csoportok(l)
    }

    def konfigMódLista = "konfig" ~ "mód" ~ ":" ~> szóLista ^^ {
      l => Konfigok(l)
    }

    def nézetMódLista = "nézet" ~ "mód" ~ ":" ~> szóLista ^^ {
      l => Nézetek(l)
    }

    def űrlapFa = "űrlapok" ~ ":" ~> rep1("+" ~> űrlap) ^^ {
      sList => ŰrlapFa("root", "", parseTree(sList))
    }

    def általánosExtraSzabályok(nézetek: Nézetek, konfigok: Konfigok) =
      űrlap ~ opt(szóListából(nézetek.l)) ~ rep(szóListából(konfigok.l)) <~ "mindenhol" ^^ {
        case űrlap ~ nézetek ~ konfigok =>
          nézetek match {
            case Some(n) =>
              ExtraSzabályok(űrlap, n, Konfigok(konfigok), Lépések(Nil), Csoportok(Nil))
            case None =>
              ExtraSzabályok(űrlap, "", Konfigok(konfigok), Lépések(Nil), Csoportok(Nil))
          }
      }

    def extraSzabályok(nézetek: Nézetek, konfigok: Konfigok, lépések: Lépések, csoportok: Csoportok) =
      űrlap ~ opt(szóListából(nézetek.l)) ~ rep(szóListából(konfigok.l)) ~ rep(szóListából(lépések.l)) ~ rep(szóListából(csoportok.l)) ^^ {
        case űrlap ~ nézetek ~ konfigok ~ lépések ~ csoportok =>
          nézetek match {
            case Some(n) =>
              ExtraSzabályok(űrlap, n, Konfigok(konfigok), Lépések(lépések), Csoportok(csoportok))
            case None =>
              ExtraSzabályok(űrlap, "", Konfigok(konfigok), Lépések(lépések), Csoportok(csoportok))
          }
      }

    def űrlapFaToList(faLista: List[ŰrlapFa], list: List[Űrlap] = Nil): List[Űrlap] = faLista match {
      case Nil => list
      case ŰrlapFa(név, szülő, gyerekek) :: többi =>
        val ez = Űrlap(név, szülő)
        űrlapFaToList(többi, űrlapFaToList(gyerekek, list ::: List(ez)))
    }

    val űrlapFaNode = """(\+*)(\+*[\w\-á-ű]+\.*\w*)""".r

    def parseTree(src: List[String], root: List[ŰrlapFa] = Nil): List[ŰrlapFa] = src match {
      case Nil => root
      case elso :: többiString =>
        elso match {
          case űrlapFaNode(pluszok, név) =>
            parseTree(többiString, addToNthChild(ŰrlapFa(név, "", Nil), pluszok.length, root))
        }
    }

    def addToNthChild(ű: ŰrlapFa, n: Int, list: List[ŰrlapFa]): List[ŰrlapFa] = n match {
      case 0 =>
        list ::: List(ű)
      case 1 =>
        val tmpLast = list.last
        val módosítottGyerek = ŰrlapFa(ű.név, tmpLast.név, ű.gyerekek)
        val módosítottSzülő = ŰrlapFa(tmpLast.név, tmpLast.szülő, tmpLast.gyerekek ::: List(módosítottGyerek))
        list.dropRight(1) ::: List(módosítottSzülő)
      case _ =>
        list.dropRight(1) ::: List(ŰrlapFa(list.last.név, list.last.szülő, addToNthChild(ű, n - 1, list.last.gyerekek)))
    }

    def szóListából(lepesek: List[String]) = szó ^? {
      case lepes if lepesek.contains(lepes) => lepes
    }
  }

  def dslRootToJsonData(rootDsl: ÜgyletAdatDSL, ugyletTípus: UgyletTipus): Root = rootDsl match {
    case ÜgyletAdatDSL(név, lépések, csoportok, konfigok, nézetek, űrlapok, _) =>
      Root(lépések.l.map(id => Lépés(id, id)), csoportok.l.map(Név), List.empty[String], List(ugyletTípus), űrlapok.l.map(ű => ŰrlapSablon(ű.név)))
  }

  def dslToData(ugyletDsl: ÜgyletAdatDSL): UgyletTipus = ugyletDsl match {
    case ÜgyletAdatDSL(Név(név), Lépések(lépések), Csoportok(csoportok), Konfigok(konfigok), Nézetek(nézetek), ŰrlapLista(űrlapok), extraSzabalyok) =>
      UgyletTipus(lépések, csoportok, név, for (Űrlap(név, szülő) <- űrlapok) yield {
        UgylettipusAdat(név, for (
          lepes <- if (lépések.nonEmpty) lépések.map(Some(_)) else List(None);
          csoport <- if (csoportok.nonEmpty) csoportok.map(Some(_)) else List(None)
        ) yield {
          val defaultKonf = (false, false, false, false, nézetek.head)
          val konf = (for (extra <- extraSzabalyok) yield {
            decodeExtraSzabalyok(név, lepes.orNull, csoport.orNull, nézetek.head, extra)
          })
            .foldLeft(defaultKonf)((x, y) => {
              (x._1 || y._1, x._2 || y._2, x._3 || y._3, x._4 || y._4,
                if (!y._1) y._5 else x._5 // ha y nem default
              )
            })
          new Konfig(
            new KonfigKey(lepes, csoport),
            new KonfigMod(konf._1, konf._2, konf._3, konf._4),
            konf._5
          )
        }, if (szülő.nonEmpty) Some(szülő) else None, if (szülő.nonEmpty) Some(szülő) else None, név)
      })
  }

  def decodeExtraSzabalyok(urlap: String, lepes: String, csoport: String, defaultNezetMod: String, szabaly: ExtraSzabályok) = szabaly match {
    case ExtraSzabályok(u, nm, km, l, cs) =>
      if (u == urlap && (l.l.isEmpty || l.l.contains(lepes)) && (cs.l.isEmpty || cs.l.contains(csoport))) {
        (km.l.contains("default"), km.l.contains("megjegyzesLathato"), km.l.contains("megjegyzesSzerkeszt"), km.l.contains("nemMentendo"), nm)
      } else {
        (false, false, false, false, defaultNezetMod)
      }
  }
}
