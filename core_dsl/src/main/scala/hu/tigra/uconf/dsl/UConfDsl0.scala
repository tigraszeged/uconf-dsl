package hu.tigra.uconf.dsl

import scala.util.parsing.combinator.JavaTokenParsers

object UConfDsl0 {

  def main(args: Array[String]): Unit = {

    val ügylet1 =
      """
        |lépések: lépésId1 lépésId2 lépésId3
        |
        |csoportok: csoportId1 csoportId2 csoportId3
        |
        |űrlapok: űrlapId1 űrlapId2 űrlapId3
        |
        |default-űrlap lépéseknek: lépésId1
        |űrlapId2 kivéve-ezeket-a-csoportokat: csoportId1 csoportId3
        |
        |default-űrlap csoportoknak: csoportId1 csoportId2
        |űrlapId1 kivéve-ezeket-a-lépéseket: lépésId2 lépésId3
        |
        |mindig L;MSZ lépéseknek: lépésId2
        |űrlapId3 kivéve-ezeket-a-csoportokat: csoportId1 csoportId3
        |
        |mindig NL csoportoknak: nem-admin-csoportId
        |adminŰrlapId kivéve-ezeket-a-lépéseket: színek-beállítása-lépésId
        |
        |""".stripMargin

    // todo: 1. fázisban: a JSON-ba kiíródik-e az űrlap és űrlap-szülője kapcsolat és ezért kell-e a DSL-be?

    // todo: 2. fázisban: wildcard csoprortID és lépésID
    // todo: 2. fázisban: #include ügylet2 mint fájl

    val ügylet2 =
      """
        |b
      """.stripMargin

    println(ügylet1)

    val startDslData = MyParser.parse(ügylet2)
    println(startDslData)
  }


  object MyParser extends JavaTokenParsers {

    def parse(s: String) = parseAll(űrlapIds(List("a", "b")), s) match {
      case Success(startDslData, _) =>
        startDslData

      case NoSuccess(msg, next) =>
        println(msg)
        println(next)
        "42"
    }

    def űrlapIds(ids: List[String]) =
      """[\w]+""".r ^? {
        case id if ids.contains(id) => id
      }
  }

}
