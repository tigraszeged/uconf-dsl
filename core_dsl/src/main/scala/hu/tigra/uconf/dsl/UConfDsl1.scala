import scala.util.parsing.combinator.JavaTokenParsers

case class ÜgyletAdatDSL(
                          név: Név,
                          lépések: Lépések,
                          csoportok: Csoportok,
                          konfigok: Konfigok,
                          nézetek: Nézetek,
                          űrlapok: ŰrlapLista,
                          extraSzabályok: List[ExtraSzabályok]
                        )

case class Név(s: String)

case class Lépések(l: List[String])

case class Csoportok(l: List[String])

case class Konfigok(l: List[String])

case class Nézetek(l: List[String])

case class ŰrlapFa(név: String, szülő: String, gyerekek: List[ŰrlapFa])

case class Űrlap(név: String, szülő: String)

case class ŰrlapLista(l: List[Űrlap])

case class ExtraSzabályok(űrlap: String, nézetMód: String, konfigMód: Konfigok, lépések: Lépések, csoportok: Csoportok)

case class UgyletTipus(nev: String, lepesek: List[String], munkatarsCsoportok: List[String], ugylettipusAdatok: List[UgylettipusAdat])

case class UgylettipusAdat(urlap: String, egyediKod: String, szulo: String, szuloEgyediKod: String, konfig: List[Konfig])

case class Konfig(konfigKey: KonfigKey, konfigMod: KonfigMod, nezetMod: String)

case class KonfigKey(munkafolyamatLepes: String, munkatarsCsoport: String)

case class KonfigMod(default: Boolean, megjegyzesLathato: Boolean, megjegyzesSzerkeszt: Boolean, nemMentendo: Boolean)

object UconfDsl1 {

  def main(args: Array[String]): Unit = {

    val test1 =
      """
        |azonosító: BESZ.
        |
        |lépések: BESZ020 BESZ030.
        |
        |munkatárs csoportok: csop2 csop3.
        |
        |konfig mód: default megjegyzesLathato megjegyzesSzerkeszt nemMentendo.
        |
        |nézet mód: Látható "Nem Látható" Módosítható.
        |
        |űrlapok:
        |    +Szülő
        |    ++Alszülő
        |    +++urlap.xhtml
        |    ++Alszülő2
        |    +++urlap2.xhtml
        |    +urlap3.xhtml
        |    +urlap4.xhtml
        |
        |urlap.xhtml default mindenhol
        |urlap2.xhtml default BESZ020
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo mindenhol
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo BESZ030
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo BESZ020 csop3
        |urlap.xhtml Módosítható megjegyzesLathato nemMentendo csop3
        |urlap.xhtml "Nem Látható" BESZ020 csop2 csop3
        |
        |""".stripMargin

    println(test1)

    val startDslData = MyParser.parse(test1)
    println(startDslData)

    val jsonData = dslToData(startDslData)
    println(jsonData)
    println("Ügylettípus adatok " + jsonData.ugylettipusAdatok.length + " db")
    println("Űrlapkonfig / ügylettípus " + jsonData.ugylettipusAdatok.head.konfig.length + " db")
  }

  object MyParser extends JavaTokenParsers {

    def parse(s: String): ÜgyletAdatDSL = parseAll(MyParser.dsl, s) match {
      case Success(startDslData, _) =>
        startDslData

      case NoSuccess(msg, next) =>
        println(msg)
        println(next)
        ÜgyletAdatDSL(null, null, null, null, null, null, null)
    }

    def sorVége = "."

    def betűk = """[\w\-á-ű]+""".r

    def szó = betűk | "\"" ~> rep1(betűk) <~ "\"" ^^ {
      list => list.mkString(" ")
    }

    def űrlap = """\+*[\w\-á-ű]+\.*\w*""".r

    def szóLista = rep1(szó) <~ sorVége

    def dsl = név ~ lépésLista ~ munkatársCsoportLista ~ konfigMódLista ~ nézetMódLista ~ űrlapFa into {
      case n ~ l ~ m ~ km ~ nm ~ űf =>
        rep(általánosExtraSzabályok(nm, km) | extraSzabályok(nm, km, l, m)) ^^ {
          eszList => ÜgyletAdatDSL(n, l, m, km, nm, ŰrlapLista(űrlapFaToList(űf.gyerekek)), eszList)
        }
    }

    def név = "azonosító" ~ ":" ~> szó <~ sorVége ^^ {
      s => Név(s)
    }

    def lépésLista = "lépések" ~ ":" ~> szóLista ^^ {
      l => Lépések(l)
    }

    def munkatársCsoportLista = "munkatárs" ~ "csoportok" ~ ":" ~> szóLista ^^ {
      l => Csoportok(l)
    }

    def konfigMódLista = "konfig" ~ "mód" ~ ":" ~> szóLista ^^ {
      l => Konfigok(l)
    }

    def nézetMódLista = "nézet" ~ "mód" ~ ":" ~> szóLista ^^ {
      l => Nézetek(l)
    }

    def űrlapFa = "űrlapok" ~ ":" ~> rep1("+" ~> űrlap) ^^ {
      sList => ŰrlapFa("root", "", parseTree(sList))
    }

    def általánosExtraSzabályok(nézetek: Nézetek, konfigok: Konfigok) =
      űrlap ~ opt(szóListából(nézetek.l)) ~ rep(szóListából(konfigok.l)) <~ "mindenhol" ^^ {
        case űrlap ~ nézetek ~ konfigok =>
          nézetek match {
            case Some(n) =>
              ExtraSzabályok(űrlap, n, Konfigok(konfigok), Lépések(Nil), Csoportok(Nil))
            case None =>
              ExtraSzabályok(űrlap, "", Konfigok(konfigok), Lépések(Nil), Csoportok(Nil))
          }
      }

    def extraSzabályok(nézetek: Nézetek, konfigok: Konfigok, lépések: Lépések, csoportok: Csoportok) =
      űrlap ~ opt(szóListából(nézetek.l)) ~ rep(szóListából(konfigok.l)) ~ rep(szóListából(lépések.l)) ~ rep(szóListából(csoportok.l)) ^^ {
        case űrlap ~ nézetek ~ konfigok ~ lépések ~ csoportok =>
          nézetek match {
            case Some(n) =>
              ExtraSzabályok(űrlap, n, Konfigok(konfigok), Lépések(lépések), Csoportok(csoportok))
            case None =>
              ExtraSzabályok(űrlap, "", Konfigok(konfigok), Lépések(lépések), Csoportok(csoportok))
          }
      }

    def űrlapFaToList(faLista: List[ŰrlapFa], list: List[Űrlap] = Nil): List[Űrlap] = faLista match {
      case Nil => list
      case ŰrlapFa(név, szülő, gyerekek) :: többi =>
        val ez = Űrlap(név, szülő)
        űrlapFaToList(többi, űrlapFaToList(gyerekek, list ::: List(ez)))
    }

    val űrlapFaNode = """(\+*)(\+*[\w\-á-ű]+\.*\w*)""".r

    def parseTree(src: List[String], root: List[ŰrlapFa] = Nil): List[ŰrlapFa] = src match {
      case Nil => root
      case elso :: többiString =>
        elso match {
          case űrlapFaNode(pluszok, név) =>
            parseTree(többiString, addToNthChild(ŰrlapFa(név, null, Nil), pluszok.length, root))
        }
    }

    def addToNthChild(ű: ŰrlapFa, n: Int, list: List[ŰrlapFa]): List[ŰrlapFa] = n match {
      case 0 =>
        list ::: List(ű)
      case 1 =>
        val tmpLast = list.last
        val módosítottGyerek = ŰrlapFa(ű.név, tmpLast.név, ű.gyerekek)
        val módosítottSzülő = ŰrlapFa(tmpLast.név, tmpLast.szülő, tmpLast.gyerekek ::: List(módosítottGyerek))
        list.dropRight(1) ::: List(módosítottSzülő)
      case _ =>
        list.dropRight(1) ::: List(ŰrlapFa(list.last.név, list.last.szülő, addToNthChild(ű, n - 1, list.last.gyerekek)))
    }

    def szóListából(lepesek: List[String]) = szó ^? {
      case lepes if lepesek.contains(lepes) => lepes
    }
  }

  def dslToData(ugyletDsl: ÜgyletAdatDSL): UgyletTipus = ugyletDsl match {
    case ÜgyletAdatDSL(Név(név), Lépések(lépések), Csoportok(csoportok), Konfigok(konfigok), Nézetek(nézetek), ŰrlapLista(űrlapok), _) =>
      UgyletTipus(név, lépések, csoportok,
        for (Űrlap(név, szülő) <- űrlapok) yield {
          UgylettipusAdat(név, név, szülő, szülő,
            for (lepes <- lépések;
                 csoport <- csoportok) yield {
              Konfig(
                KonfigKey(lepes, csoport),
                KonfigMod(false, false, false, false),
                nézetek.head
              )
            }
          )
        }
      )
  }
}
